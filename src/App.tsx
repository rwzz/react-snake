import React, { useReducer, useEffect } from 'react'
import './App.css'

interface Settings {
  width: number
  height: number
}

const DefaultSettings: Settings = {
  width: 35,
  height: 35,
}

interface Coordinates {
  x: number
  y: number
}

enum Direction {
  Up = 'up',
  Right = 'right',
  Down = 'down',
  Left = 'left',
}

const Controls: { [index: string]: Direction } = {
  ArrowUp: Direction.Up,
  ArrowRight: Direction.Right,
  ArrowLeft: Direction.Left,
  ArrowDown: Direction.Down,

  w: Direction.Up,
  a: Direction.Left,
  s: Direction.Down,
  d: Direction.Right,
}

enum Cell {
  Empty = 'empty',
  Snake = 'snake',
  Food = 'food',
}

class Grid {
  readonly settings: Settings
  private grid: Cell[]

  constructor(settings: Settings) {
    this.settings = settings
    this.grid = Array(settings.height * settings.width).fill(Cell.Empty)
  }

  at(c: Coordinates): Cell {
    return this.grid[this.toIndex(c)]
  }

  outOfBounds(c: Coordinates): boolean {
    return (
      c.x < 0 ||
      c.y < 0 ||
      c.x >= this.settings.width ||
      c.y >= this.settings.height
    )
  }

  set(c: Coordinates, value: Cell): void {
    this.grid[this.toIndex(c)] = value
  }

  findEmptyCell(): Coordinates {
    const candidate = {
      x: Math.floor(Math.random() * this.settings.width),
      y: Math.floor(Math.random() * this.settings.height),
    }

    return this.at(candidate) === Cell.Empty ? candidate : this.findEmptyCell()
  }

  private toIndex(c: Coordinates): number {
    if (this.outOfBounds(c)) {
      throw new Error('Out of bounds')
    }

    return c.y * this.settings.width + c.x
  }
}

interface StateProps {
  direction: Direction
  nextDirection: Direction | null,
  changedDirection: boolean,
  settings: Settings
  snake: Coordinates[]
  food: Coordinates[]
  score: number
  highScore: number
  lost: boolean
}

class State {
  readonly direction: Direction
  readonly nextDirection: Direction | null
  readonly changedDirection: boolean
  readonly snake: Coordinates[]
  readonly food: Coordinates[]
  readonly score: number
  readonly highScore: number
  readonly grid: Grid
  readonly lost: boolean

  constructor({
    direction,
    nextDirection,
    changedDirection,
    settings,
    snake,
    food,
    score,
    highScore,
    lost,
  }: StateProps) {
    this.direction = direction
    this.nextDirection = nextDirection
    this.changedDirection = changedDirection
    this.score = score
    this.highScore = highScore
    this.lost = lost
    this.snake = snake
    this.grid = new Grid(settings)

    for (let c of snake) {
      this.grid.set(c, Cell.Snake)
    }

    this.food = this.prepareFood(food)
  }

  static MIN_TICK: number = 50 // ms
  static TICK: number = 300 // ms

  canAcceptDirection(direction: Direction): boolean {
    return this.canChangeCurrentDirection(direction) || !this.changedDirection;
  }

  pushDirection(direction: Direction): State {
    if (this.changedDirection) {
      return this.with({nextDirection: direction})
    } else {
      return this.with({direction: direction, changedDirection: true, nextDirection: null})
    }
  }

  private canChangeCurrentDirection(direction: Direction): boolean {
    if (direction === this.direction) {
      return false
    }

    if (this.snake.length === 1) {
      return true
    }

    // When snake is longer than one, 180 turns into itself become impossible
    return this.grid.outOfBounds(this.nextHead) || this.grid.at(this.nextHead) !== Cell.Snake
  }

  // runs on every tick
  advance(): State {
    let { direction, nextDirection, snake, score, highScore } = this
    const newHead = this.nextHead

    if (
      this.grid.outOfBounds(newHead) ||
      this.grid.at(newHead) === Cell.Snake
    ) {
      return this.with({ lost: true })
    }

    snake = [newHead, ...snake]

    if (this.grid.at(newHead) === Cell.Food) {
      score += 1
      highScore = Math.max(highScore, score)
    } else {
      snake.pop()
    }

    const changedDirection = false

    if (nextDirection) {
      direction = nextDirection
      nextDirection = null
    }

    return this.with({ direction, nextDirection, changedDirection, snake, score, highScore })
  }

  with(overrides: {}): State {
    return new State({
      ...this,
      ...{ settings: this.grid.settings },
      ...overrides,
    })
  }

  get tick(): number {
    return Math.max(
      State.MIN_TICK,
      State.TICK * Math.pow(0.95, this.score)
    )
  }

  get requiredFoodQty(): number {
    if (this.score < 5) {
      return 1
    } else if (this.score < 15) {
      return 2
    } else {
      return 3
    }
  }

  private prepareFood(initialFood: Coordinates[]): Coordinates[] {
    let generatedFood = []

    for (let c of initialFood) {
      if (this.grid.at(c) !== Cell.Snake) {
        generatedFood.push(c)
        this.grid.set(c, Cell.Food)
      }
    }

    while (generatedFood.length < this.requiredFoodQty) {
      const c = this.grid.findEmptyCell()
      generatedFood.push(c)
      this.grid.set(c, Cell.Food)
    }

    return generatedFood
  }

  private get head(): Coordinates {
    return this.snake[0]
  }

  private get nextHead(): Coordinates {
    return this.move(this.head, this.direction)
  }

  private move(from: Coordinates, direction: Direction): Coordinates {
    switch (direction) {
      case Direction.Up: {
        return {
          ...from,
          y: from.y - 1,
        }
      }
      case Direction.Right: {
        return {
          ...from,
          x: from.x + 1,
        }
      }
      case Direction.Down: {
        return {
          ...from,
          y: from.y + 1,
        }
      }
      case Direction.Left: {
        return {
          ...from,
          x: from.x - 1,
        }
      }
    }
  }
}

const initialState = new State({
  direction: Direction.Right,
  nextDirection: null,
  changedDirection: false,
  settings: DefaultSettings,
  snake: [
    {
      x: Math.floor(DefaultSettings.width / 2),
      y: Math.floor(DefaultSettings.height / 2),
    },
  ],
  food: [],
  score: 0,
  highScore: 0,
  lost: false,
})

type Action =
  | { type: 'push-direction'; direction: Direction }
  | { type: 'tick' }
  | { type: 'reset' }

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'push-direction': {
      return state.pushDirection(action.direction)
    }

    case 'tick': {
      return state.advance()
    }

    case 'reset': {
      return initialState.with({ highScore: state.highScore })
    }
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, initialState)

  const handleKeypress = (event: KeyboardEvent): void => {
    const direction = Controls[event.key]

    if (direction !== undefined && state.canAcceptDirection(direction)) {
      dispatch({ type: 'push-direction', direction })
    }
  }

  useEffect(() => {
    window.addEventListener('keydown', handleKeypress, false)
    return () => {
      window.removeEventListener('keydown', handleKeypress, false)
    }
  })

  useEffect(() => {
    let interval: NodeJS.Timeout | undefined

    if (!state.lost) {
      interval = setInterval(() => {
        dispatch({ type: 'tick' })
      }, state.tick)
    }

    return () => {
      if (interval) {
        clearInterval(interval)
      }
    }
  }, [state.lost, state.tick])

  const { grid } = state
  const { settings } = grid

  let cells = []

  for (let y = 0; y < settings.height; y++) {
    for (let x = 0; x < settings.width; x++) {
      cells.push(
        <div className={`cell ${grid.at({ x, y })}`} key={`${x}-${y}`}></div>,
      )
    }
  }

  return (
    <div className="App">
      <h1>Snake!</h1>
      <div className="score">Score: {state.score}</div>
      <div className="high-score">High Score: {state.highScore}</div>
      <div
        className="grid"
        style={{ gridTemplateColumns: `repeat(${settings.width}, 21px)` }}
      >
        {cells}
      </div>
      {state.lost && (
        <div className="game-over">
          <h2>Game Over!</h2>
          <button
            className="retry"
            onClick={() => {
              dispatch({ type: 'reset' })
            }}
          >
            Try again!
          </button>
        </div>
      )}
      <footer>
        Source code is available at:{' '}
        <a href="https://gitlab.com/rwzz/react-snake">
          https://gitlab.com/rwzz/react-snake
        </a>
      </footer>
    </div>
  )
}

export default App
